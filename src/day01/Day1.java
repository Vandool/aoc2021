package day01;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Day1 {

    static void part2(ArrayList<Integer> list) {
        // pass the path to the file as a parameter

        int count = 0;
        for (int i = 3; i < list.size(); i++) {
            if (list.get(i) > list.get(i - 3)) {
                count++;
            }
        }

        System.out.println("part2:" + count);
    }

    public static void main(String[] args) throws Exception
    {
        // pass the path to the file as a parameter
        File file = new File(".\\src\\day01\\input.txt");
        Scanner sc = new Scanner(file);

        ArrayList<Integer> list = new ArrayList<Integer>();

        while (sc.hasNextInt()) {
            list.add(sc.nextInt());
        }

        sc.close();

        part2(list);
    }
}
