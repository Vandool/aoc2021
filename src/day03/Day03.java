package day03;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day03 {

    public static final char CHAR_ZERO = '0';
    private static final int ZERO = 0;

    public static void main(String[] args) throws FileNotFoundException {

        // pass the path to the file as a parameter
        File file = new File(".\\src\\day03\\input");
        Scanner sc = new Scanner(file);

        ArrayList<String> list = new ArrayList<>();
        while (sc.hasNextLine()) {
            list.add(sc.nextLine());
        }

        part01(list);
        part02(list);

        sc.close();
    }

    private static void part01(@NotNull ArrayList<String> list) {
        int[] gammaDigits = new int[list.get(ZERO).length()];
        for (var s : list) {
            for (int j = 0; j < gammaDigits.length; j++) {
                gammaDigits[j] += s.charAt(j) - CHAR_ZERO;
            }
        }

        StringBuilder gammaSb = new StringBuilder();
        StringBuilder epsilonSb = new StringBuilder();

        for (var gammaDigit : gammaDigits) {
            gammaSb.append((gammaDigit > list.size() / 2) ? 1 : 0);
            epsilonSb.append((gammaDigit > list.size() / 2) ? 0 : 1);
        }

        //gamma * epsilon
        System.out.printf("Day03 Part01: %n%d%n",
            Integer.parseInt(gammaSb.toString(), 2)
                * Integer.parseInt(epsilonSb.toString(), 2)
        );
    }

    private static void part02(ArrayList<String> list) {
        // oxygen * co2
        System.out.printf(
            "Day03 Part02: %n%d%n",
            getValue(list, 0)
                * getValue(list, 1));
    }

    private static int getValue(@NotNull ArrayList<String> list, int preferedBinary) {
        List<Integer> validIndexes = IntStream.rangeClosed(0, list.size() - 1)
            .boxed().collect(Collectors.toList());

        int sumOfBits = 0;
        for (int i = 0; i < list.get(ZERO).length(); i++) {

            List<Integer> temp = new ArrayList<>();
            for (var j : validIndexes) {
                int val = list.get(j).charAt(i) - CHAR_ZERO;
                sumOfBits += val;
                if (val == preferedBinary) temp.add(j);
            }

            int indexesOldSize = validIndexes.size();
            if (2 * sumOfBits >= indexesOldSize) {
                validIndexes.clear();
                validIndexes.addAll(temp);
            } else {
                validIndexes.removeAll(temp);
            }

            sumOfBits = 0;
            if (validIndexes.size() == 1) break;
        }

        return Integer.parseInt(list.get(validIndexes.get(0)), 2);
    }
}
