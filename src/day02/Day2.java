package day02;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Day2 {
    public static void main(String[] args) throws FileNotFoundException {
        // pass the path to the file as a parameter
        File file = new File(".\\src\\day02\\input");
        Scanner sc = new Scanner(file);

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("forward", 0);
        map.put("down", 0);
        map.put("up", 0);

        int horizontal = 0;
        int vertical = 0;
        int aim = 0;
        while (sc.hasNextLine()) {
            String[] s = sc.nextLine().split(" ");

            if (s[0].equals("forward")) {

                horizontal += (int) Integer.parseInt(s[1]);
                vertical += (aim * Integer.parseInt(s[1]));
            }

            if (s[0].equals("down")) aim += (int) Integer.parseInt(s[1]);
            if (s[0].equals("up")) aim -= (int) Integer.parseInt(s[1]);
        }

        System.out.println("part1: " + (horizontal * vertical));
        System.out.printf("vertical: %d, horizontal: %d%n", vertical, horizontal);

        sc.close();
    }

    private static void part1(@NotNull HashMap<String, Integer> map) {
        int vertical = 0;
        int horizontal = 0;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue().equals("down")) {
                vertical += entry.getValue();
            }
            if (entry.getValue().equals("up")) {
                vertical -= entry.getValue();
            }
            if (entry.getValue().equals("forward")) {
                horizontal += entry.getValue();
            }

        }

        System.out.println("part1: " + horizontal * vertical);
    }
}
